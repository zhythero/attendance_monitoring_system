<?php 

class Admin extends CI_Model {

	protected $table = 'admins';

	public function authenticate($username, $password) {

		$result = $this->db->get_where( $this->table, ['username' => $username] );

		if ($result->num_rows() == 1) {

			$admin = $result->result_array()[0];

			return $admin['password'] == md5($password);

		} else {
			return false;
		}

	}

}

 ?>