<?php 

class Faculty extends CI_Model {

	protected $table = 'faculties';

	public function authenticate($id, $password) {

		$result = $this->db->get_where( $this->table, ['id' => $id] );

		if ($result->num_rows() == 1) {

			$admin = $result->result_array()[0];

			return $admin['password'] == md5($password);

		} else {
			return false;
		}

	}

}

 ?>