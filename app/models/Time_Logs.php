<?php

class Time_Logs extends CI_Model {

  protected $table = 'time_logs';

  public function get_bimonthly($faculty_id, $year = null, $month = null, $month_half = null) {

    $this->load->library('Date', 'date');
    $this->load->model('Dates', 'dates');

    // Set default parameters
    $year = (!is_null($year)) ? $year : date('Y');
    $month = (!is_null($month)) ? $month : date('M');
    $month_half = (!is_null($month_half)) ? $month_half : $this->date->get_current_month_half();

    // Set date range
    $date_range = $this->date->get_date_range($year, $month, $month_half);
    $date_range_where = "date BETWEEN STR_TO_DATE('".$date_range['from']."', '%Y-%m-%d') AND STR_TO_DATE('".$date_range['to']."', '%Y-%m-%d')";

    // Get Dates
    $this->dates->set_limit(15);
    $dates = $this->dates->get('date', $date_range_where);

    // Get Time Logs
    $time_logs = $this->get(['date', 'time', 'period', 'type'], [
      'faculty_id' => $faculty_id,
      1 => $date_range_where
    ]);

    // Merge Dates and Time Logs
    for ($date_index=0; $date_index < count($dates); $date_index++) {

      if (!empty($time_logs)) {
        for ($timelog_index=0; $timelog_index < count($time_logs); $timelog_index++) {

          if ($dates[$date_index]['date'] == $time_logs[$timelog_index]['date']) {
            $bimonth_time_logs[$date_index]['date'] = $dates[$date_index]['date'];
            $bimonth_time_logs[$date_index]['time_logs'][ $time_logs[$timelog_index]['period'] ][ $time_logs[$timelog_index]['type'] ] = $time_logs[$timelog_index]['time'];
          } else {
            $bimonth_time_logs[$date_index]['date'] = $dates[$date_index]['date'];
          }

        }
      } else {
        $bimonth_time_logs[$date_index]['date'] = $dates[$date_index]['date'];
        $bimonth_time_logs[$date_index]['time_logs'] = [];
      }

    }

    return $bimonth_time_logs;
  }

  public function check_if_can_time($log_type) {

    // Identify period
    $this->load->library('Date', 'date');
    $period = $this->date->get_period();

    // Check for existing record
    $hasRecord = $this->count([
      'faculty_id' => $_SESSION['faculty_id'],
      'date' => date('Y-m-d'),
      'period' => $period,
      'type' => $log_type
    ]);

    switch ($log_type) {
      case 'in':
        return $hasRecord < 1;
        break;
      case 'out':

        if ($hasRecord < 1) {

          // Check if has logged in
          $hasLoggedIn = $this->count([
            'faculty_id' => $_SESSION['faculty_id'],
            'date' => date('Y-m-d'),
            'period' => $period,
            'type' => 'in'
          ]);

          return $hasLoggedIn >= 1;
        }

        return false;

        break;
    }
  }

  public function time_in() {

    // Identify period
    $this->load->library('Date', 'date');
    $period = $this->date->get_period();

    if ($this->check_if_can_time('in')) {
      $this->create([
        'faculty_id' => $_SESSION['faculty_id'],
        'date' => date('Y-m-d'),
        'time' => date('H:i:s'),
        'period' => $period,
        'type' => 'in'
      ]);

      return true;
    }

    return false;
  }

  public function time_out() {

    // Identify period
    $this->load->library('Date', 'date');
    $period = $this->date->get_period();

    if ($this->check_if_can_time('out')) {

      $this->create([
        'faculty_id' => $_SESSION['faculty_id'],
        'date' => date('Y-m-d'),
        'time' => date('H:i:s'),
        'period' => $period,
        'type' => 'out'
      ]);

      return true;
    }

    return false;
  }

}

 ?>
