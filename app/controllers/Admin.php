<?php 

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Validate session
		if ( !isset( $_SESSION['admin_id'] ) ) {
			redirect('auth/login');
		}
	}

	public function index() {
		redirect('admin/faculties');
	}

	public function faculties() {

		$view_data['page_title'] = 'Faculties';

		$this->load->view('_globals/_head', $view_data);
		$this->load->view('admin/_navbar');
		$this->load->view('admin/faculties');
		$this->load->view('_globals/_foot');

	}

}

 ?>