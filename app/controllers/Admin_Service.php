<?php

class Admin_Service extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->library('Responser', 'responser');

		// Validate session
		if ( !isset( $_SESSION['admin_id'] ) ) {
			$this->responser->respond(false, 'Unauthenticated');
			exit;
		}
	}

	public function index() {

		$this->responser->respond(false, 'No resource requested.');

	}

	public function faculties($method = '') {

		$this->load->model('Faculty', 'faculty');

		switch ($method) {

			// Create Faculty
			case 'create':

				// JSON Decode the fields then put fields to $_POST
				foreach (json_decode($_POST['form_data']) as $key => $value) {
					$_POST[$key] = $value;
				} unset($_POST['form_data']);

				$this->load->library('form_validation');

				$this->form_validation->set_rules('id', 'Faculty ID', 'required|alpha_dash|callback_is_unique_faculty_id', [
					'is_unique_faculty_id' => 'Faculty ID already exists.'
					]);
				$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
				$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
				$this->form_validation->set_rules('fname', 'First Name', 'required|alpha_numeric_spaces');
				$this->form_validation->set_rules('mname', 'Middle Name', 'alpha_numeric_spaces');
				$this->form_validation->set_rules('lname', 'Last Name', 'required|alpha_numeric_spaces');
				$this->form_validation->set_rules('degree', 'Degree', 'required|alpha_dash');
				$this->form_validation->set_rules('department', 'Department', 'required|alpha_dash');
				$this->form_validation->set_rules('work_shift', 'Work Shift', 'required|in_list[organic,non-organic]');

				if ($this->form_validation->run()) {

					$config = [
						'upload_path' => './assets/img/faculties',
						'allowed_types' => 'jpg|png',
						'file_name' => $_POST['id'] . '_' . uniqid()
					];

					$this->load->library('upload', $config);

					if (! $this->upload->do_upload('file') ) {
						$this->responser->respond(false, [
							'errors' => $this->upload->display_errors(' ', ' ')
						]);
					}

					// Blank middle name if it doesn't exist
					if (!isset($_POST['mname'])) $_POST['mname'] = '';

					$this->load->model('Faculty');
					$success = $this->Faculty->create([
							'id' => $_POST['id'],
							'password' => md5($_POST['password']),
							'fname' => ucfirst($_POST['fname']),
							'mname' => ucfirst($_POST['mname']),
							'lname' => ucfirst($_POST['lname']),
							'degree' => $_POST['degree'],
							'department' => $_POST['department'],
							'work_shift' => $_POST['work_shift'],
							'img_url' => $this->upload->data('file_name')
						]);

					if ($success)
						$this->responser->respond(true, ['message' => 'Successfully created faculty.']);
					else
						$this->responser->respond(false, ['message' => 'Something went wrong while creating faculty.']);

				} else {
					$this->responser->respond(false, [
							'errors' => $this->form_validation->error_string(' ', ' ')
						]);
				}

				break;

			// Get faculties
			case 'get':

				$this->responser->respond(true, $this->faculty->get());

				break;
		}

	}

	public function is_unique_faculty_id() {

		$this->load->model('Faculty', 'faculty');
		$count = $this->faculty->count(['id' => $_POST['id']]);

		return $count < 1;

	}

}

 ?>
