<?php 

class Auth extends CI_Controller {

	private function user_must_not_be_logged_in() {
		if ( isset($_SESSION['admin_id']) ) {

			redirect('admin');

		} else if ( isset($_SESSION['faculty_id']) ) {

			redirect('faculty');

		}
	}

	public function index() {
		$this->user_must_not_be_logged_in();
		redirect('auth/login');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('auth/login');
	}

	public function login() {
		
		$this->user_must_not_be_logged_in();
	
		$this->load->library('form_validation');

		$this->form_validation->set_rules('user_type', 'User Type', 'required');
		$this->form_validation->set_rules('username', 'Faculty ID/Admin Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_authenticate', ['authenticate' => 'Invalid Credentials.']);


		if ( $this->form_validation->run() ) {

			if ($_POST['user_type'] == 'admin') {

				$this->load->model('Admin', 'admin');
				$admin_id = $this->admin->get('id', ['username' => $_POST['username']])[0]['id'];

				$this->session->set_userdata('admin_id', $admin_id);

				redirect('admin');

			} else if ($_POST['user_type'] == 'faculty') {

				$this->session->set_userdata('faculty_id', $_POST['username']);

				redirect('faculty');

			}

		} else {

			$view_data['page_title'] = 'Please Login';
			$view_data['errors'] = $this->form_validation->error_array();

			$this->load->view('_globals/_head', $view_data);
			$this->load->view('auth/login');
			$this->load->view('_globals/_foot');
		}

	}

	public function authenticate() {

		$this->user_must_not_be_logged_in();

		if ($_POST['user_type'] == 'admin') {

			$this->load->model('Admin', 'admin');
			return $this->admin->authenticate($_POST['username'], $_POST['password']);

		} else if ($_POST['user_type'] == 'faculty') {

			$this->load->model('Faculty', 'faculty');
			return $this->faculty->authenticate($_POST['username'], $_POST['password']);

		}

	}

}

 ?>