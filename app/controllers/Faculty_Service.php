<?php

class Faculty_Service extends CI_Controller {

  public function __construct() {
		parent::__construct();

    $this->load->library('Responser', 'responser');
    $this->load->library('Http', 'http');

		// Validate session
		if ( !isset( $_SESSION['faculty_id'] ) ) {
			$this->responser->respond(false, 'Unauthenticated');
			exit;
		}

	}

  public function index() {
    $this->responser->respond(false, 'No resource requested');
  }

  public function me() {
    $this->load->model('Faculty', 'faculty');
    $me = $this->faculty->get([
      'id', 'fname','mname',
      'lname','img_url','degree',
      'department','work_shift'
    ], ['id' => $_SESSION['faculty_id']])[0];

    $this->responser->respond(true, $me);
  }

  public function time_logs($method = 'get') {
    $this->load->model('Time_Logs', 'timelogs');

    switch($method) {

      case 'get':
        $this->http->limit_method('GET');

        $timelogs = $this->timelogs->get_bimonthly($_SESSION['faculty_id']);
        $this->responser->respond(true, $timelogs);
        break;

      case 'time_in':

        $this->http->limit_method('POST');

        $is_success = $this->timelogs->time_in();

        if ($is_success)
          $this->responser->respond(true, 'Successfully timed in.');
        else
          $this->responser->respond(false, 'You already have time in record.');

        break;

      case 'time_out':

        $this->http->limit_method('POST');

        $is_success = $this->timelogs->time_out();

        if ($is_success)
          $this->responser->respond(true, 'Successfully timed out.');
        else
          $this->responser->respond(false, 'You already have time out record or you may have not timed in yet.');

        break;

      case 'get_available_time_log_methods':
        $this->http->limit_method('GET');

        $this->responser->respond(true, [
          'time_in' => $this->timelogs->check_if_can_time('in'), 
          'time_out' => $this->timelogs->check_if_can_time('out')
          ]);

        break;
    }

  }

}

 ?>
