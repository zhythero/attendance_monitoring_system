<?php

class Faculty extends CI_Controller {

	public function __construct() {
		parent::__construct();

		// Validate session
		if ( !isset( $_SESSION['faculty_id'] ) ) {
			redirect('auth/login');
		}
	}

	public function index() {
		redirect('faculty/dashboard');
	}

	public function dashboard() {

		$this->load->view('_globals/_head');
		$this->load->view('faculty/dashboard');
		$this->load->view('_globals/_foot');

	}

}

 ?>
