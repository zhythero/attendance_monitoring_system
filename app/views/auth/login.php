<div class="container" style="margin-top: 100px;">
	<div class="row">
		<div class="col-md-offset-4 col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h6><i class="fa fa-lock"></i> Attendance Monitoring System Login</h6>
				</div>
				<div class="panel-body">
					<?php if ( !empty($errors) ): ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">
								<span>&times;</span>
							</button>
							<?php foreach ($errors as $error): ?>
								<p><?php echo $error; ?></p>
							<?php endforeach ?>
						</div>
					<?php endif ?>
					<form method="POST" action="">
						<div class="form-group">
							<label>Account Type</label>
							<select class="form-control" name="user_type">
								<option value="faculty">Faculty</option>
								<option value="admin">Admin</option>
							</select>
						</div>
						<div class="form-group">
							<label>Admin Username/Faculty ID</label>
							<input class="form-control" name="username" placeholder="Admin Username/Faculty ID" />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control" placeholder="Password" >
						</div>
						<button class="btn btn-primary">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
