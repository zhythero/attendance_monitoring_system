<!DOCTYPE html>
<html ng-app="attendanceMonitoringSystem">
<head>
	<title>
		<?php echo ( isset($page_title) ) ? $page_title : 'A.M.S.'; ?>
	</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paper.min.css'); ?>">

	<script src="<?php echo base_url('assets/js/jquery-2.2.3.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/angular.min.js'); ?>"></script>

	<!-- Base URL -->
	<script>
	var baseUrl = '<?php echo base_url("index.php"); ?>/';
	var adminServiceUrl = baseUrl + 'admin_service';
	var facultyServiceUrl = baseUrl + 'faculty_service';
	</script>

	<!-- App -->
	<script src="<?php echo base_url('angular/app.js'); ?>"></script>

	<!-- App Dependencies -->
	<script src="<?php echo base_url('assets/js/angular-file-upload.min.js'); ?>"></script>

	<!-- Services and Directives -->
	<script src="<?php echo base_url('angular/directives/fileUpload.js'); ?>"></script>

</head>
<body>
