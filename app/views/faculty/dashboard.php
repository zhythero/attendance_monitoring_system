<script src="<?php echo base_url('angular/controllers/faculty/dashboard.js'); ?>"></script>

<section ng-controller="dashboard" ng-init="getMyData();">
	<div class="jumbotron" style="background-color: #1A237E; color: #fff">
		<div class="container">
			<div class="row">
				<p class="text-right" style="color: #aaa; font-size: 110%">
					Attendance Monitoring System
				</p>
			</div>
			<div class="row">

				<div class="col-md-offset-3 col-md-6">
					<h1 class="text-center" style="color: #fff;">
						<i class="fa fa-clock-o"></i> {{ currentTime }}
					</h1>
					<h3 class="text-center" style="color: #fff; margin-top: -10px; margin-bottom: 20px">
						{{ currentDate }}
					</h3>
					<p class="text-center"><small><i class="fa fa-user"></i> {{ me.lname }}, {{ me.fname }} {{ me.mname }}</small></p>
					<div class="btn-group btn-group-justified" ng-init="getAvailableTimeLogMethods();">
						<div class="btn-group">
							<button type="button" class="btn btn-success" ng-disabled="canTime.in" ng-click="timeIn()">
								<i class="fa fa-fw fa-sign-in"></i> Time In
							</button>
						</div>
						<div class="btn-group">
							<button type="button" class="btn btn-danger" ng-disabled="canTime.out" ng-click="timeOut()">
								<i class="fa fa-fw fa-sign-out"></i> Time Out
							</button>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-user"></i> About me
					</div>
					<div class="panel-body">
						<img ng-src="<?php echo base_url('assets/img/faculties'); ?>/{{ me.img_url }}" class="img-responsive" style="max-height: 150px; margin: auto; margin-bottom: 10px">

						<table class="table table-striped">
							<tbody>
								<tr>
									<th>Faculty ID</th>
									<td>{{ me.id }}</td>
								</tr>
								<tr>
									<th>First Name</th>
									<td>{{ me.fname }}</td>
								</tr>
								<tr>
									<th>Middle Name</th>
									<td>{{ me.mname }}</td>
								</tr>
								<tr>
									<th>Last Name</th>
									<td>{{ me.lname }}</td>
								</tr>
								<tr>
									<th>Degree</th>
									<td>{{ me.degree }}</td>
								</tr>
								<tr>
									<th>Department</th>
									<td>{{ me.department }}</td>
								</tr>
								<tr>
									<th>Work Shift</th>
									<td>{{ me.work_shift }}</td>
								</tr>
							</tbody>
						</table>
						<a href="<?php echo base_url('index.php/auth/logout'); ?>" class="btn btn-default pull-right">
							<i class="fa fa-sign-out"></i> Logout
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4><i class="fa fa-calendar"></i> Current cut-off</h4>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th class="text-center col-md-1" style="vertical-align: middle" rowspan="2">Date</th>
									<th class="text-center" colspan="2">AM</th>
									<th class="text-center" colspan="2">PM</th>
									<th class="text-center" colspan="2">OVERTIME</th>
								</tr>
								<tr>
									<th class="text-center col-md-1">IN</th>
									<th class="text-center col-md-1">OUT</th>
									<th class="text-center col-md-1">IN</th>
									<th class="text-center col-md-1">OUT</th>
									<th class="text-center col-md-1">IN</th>
									<th class="text-center col-md-1">OUT</th>
								</tr>
							</thead>
							<tbody ng-init="timeLogs.get()">
								<tr ng-repeat="timeLog in timeLogs.collection" ng-class="{'active' : isCurrentDay(timeLog.date)}">
									<td>{{ toDate(timeLog.date) | date : 'yyyy, MMMM d' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.am.in) | date : 'hh:mm a' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.am.out) | date : 'hh:mm a' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.pm.in) | date : 'hh:mm a' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.pm.out) | date : 'hh:mm a' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.overtime.in) | date : 'hh:mm a' }}</td>
									<td class="text-center">{{ toTime(timeLog.time_logs.overtime.out) | date : 'hh:mm a' }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="panel-footer text-right">
						<button class="btn btn-default btn-lg" onClick="window.print()" type="button"><i class="fa fa-print"></i> Print</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
