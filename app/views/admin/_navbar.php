<div class="navbar navbar-inverse">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">A.M.S. Admin Panel</a>
		</div>
		<ul class="nav navbar-nav navbar-left">
			<li class="active"><a href="<?php echo base_url('admin/faculties'); ?>">Faculties</a></li>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="<?php echo base_url('index.php/auth/logout'); ?>">Logout</a></li>
		</ul>
	</div>
</div>