<script src="<?php echo base_url('angular/controllers/admin/faculties.js'); ?>"></script>

<section ng-controller="facultiesController" ng-init="get()">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h3>Faculties</h3>
				<table class="table table-striped">
					<thead>
						<tr>
							<th></th>
							<th>ID</th>
							<th>First Name</th>
							<th>Middle Name</th>
							<th>Last Name</th>
							<th>Degree</th>
							<th>Department</th>
							<th>Work Shift</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="faculty in collection">
							<td>
								<img ng-src="<?php echo base_url('assets/img/faculties'); ?>/{{ faculty.img_url }}" style="width: 40px; height: 40px;margin: 0 auto" />
							</td>
							<td>{{ faculty.id }}</td>
							<td>{{ faculty.fname }}</td>
							<td>{{ faculty.mname }}</td>
							<td>{{ faculty.lname }}</td>
							<td>{{ faculty.degree }}</td>
							<td>{{ faculty.department }}</td>
							<td>{{ faculty.work_shift }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-3" style="padding-top: 50px">
				<button class="btn btn-block btn-default" data-toggle="modal" data-target="#create_faculty_modal"><i class="fa fa-user-plus"></i> New Faculty</button>
			</div>
		</div>
	</div>

	<!-- Create new faculty modal -->
	<div class="modal fade" id="create_faculty_modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h4><i class="fa fa-user-plus"></i> Create new faculty</h4>
				</div>
				<div class="modal-body">
					<div ng-class="getCreateFacultyResponseClass(createFacultyResponse.success)" style="padding-top: 0; white-space: pre-line">
						{{ createFacultyResponse.message }}
					</div>
					<form onsubmit="return false">
						<div class="row">
							<div class="col-md-3" style="overflow: hidden;">
								<div class="form-group">
									<label for="">Upload Picture</label>
									<input type="file" file-upload />
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Faculty ID</label>
									<input type="text" class="form-control" ng-model="createFacultyFormData.id" placeholder="Faculty ID">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" class="form-control" ng-model="createFacultyFormData.password" placeholder="Password">
								</div>
								<div class="form-group">
									<label>Confirm Password</label>
									<input type="password" class="form-control" ng-model="createFacultyFormData.passconf" placeholder="Confirm Password">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>First Name</label>
									<input type="text" ng-model="createFacultyFormData.fname" class="form-control" placeholder="First Name">
								</div>
								<div class="form-group">
									<label>Middle Name</label>
									<input type="text" ng-model="createFacultyFormData.mname" class="form-control" placeholder="Middle Name">
								</div>
								<div class="form-group">
									<label>Last Name</label>
									<input type="text" ng-model="createFacultyFormData.lname" class="form-control" placeholder="Last Name">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label>Degree</label>
									<input type="text" class="form-control" ng-model="createFacultyFormData.degree" placeholder="Degree">
								</div>
								<div class="form-group">
									<label>Department</label>
									<input type="text" class="form-control" ng-model="createFacultyFormData.department" placeholder="Department">
								</div>
								<div class="form-group">
									<label>Work Shift</label>
									<select class="form-control" ng-model="createFacultyFormData.work_shift">
										<option value="organic" selected>Organic</option>
										<option value="non-organic">Non-organic</option>
									</select>
								</div>
							</div>
						</div>
						<button class="btn btn-primary" ng-click="create()" ng-disabled="isCreating">Create</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
