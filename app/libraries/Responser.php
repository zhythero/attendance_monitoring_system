<?php

class Responser {

  private $mime_type = 'application/json';

  public function respond($success, $data) {

		header('Content-Type: ' . $this->mime_type);

		$response = [
			'success' => $success,
			'data' => $data
		];

		echo json_encode($response, JSON_PRETTY_PRINT);

    exit;
	}

}

 ?>
