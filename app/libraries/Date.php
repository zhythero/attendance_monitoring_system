
<?php

class Date {

  public function get_current_month_half() {
    return ( intval(date('d')) <= 15) ? 1 : 2;
  }

  public function get_date_range($year, $month, $month_half) {

    if ($month_half == 1) {
      $date_range['from'] = date( 'Y-m-d', strtotime("First day of $month $year") );
      $date_range['to'] = date( 'Y-m-d', strtotime("$month 15 $year") );
    } else if ($month_half == 2) {
      $date_range['from'] = date( 'Y-m-d', strtotime("$month 16 $year") );
      $date_range['to'] = date( 'Y-m-d', strtotime("Last day of $month $year") );
    }

    return $date_range;
  }

  public function get_period($time = null) {

    $base_date = 'Jan 01 1970 ';

    if (is_null($time)) {
      $time = date('H');
    } else if (is_int($time)) {
      $time = date('H'); 
    } else {
      $time = date('H', strtotime($time));
    }

    $time = intval($time);

    if ($time < 12) {
      $period = 'am';
    } else if ($time >= 12 && $time < 17) {
      $period = 'pm';
    } else {
      $period = 'overtime';
    }

    return $period;

  }

}

 ?>
