<?php

function pre($data, $exit = false) {
  echo '<pre>';
  print_r($data);
  echo '</pre>';

  if ($exit) exit;
}

function look($data, $exit = false) {
  echo '<pre>';
  var_dump($data);
  echo '</pre>';

  if ($exit) exit;
}

 ?>
