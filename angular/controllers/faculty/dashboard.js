app.controller('dashboard', function($http, $scope, $interval) {

  // Date
  $interval(function() {
    $scope.currentDateTime = new Date();
    $scope.currentTime = $scope.currentDateTime.toLocaleTimeString();
    $scope.currentDate = $scope.currentDateTime.toDateString();
  }, 900);

  $scope.me = {};

  $scope.getMyData = function() {
    $http({
      method : 'GET',
      url : facultyServiceUrl + '/me'
    }).then(function(response) {

      if (response.data.success) {
        $scope.me = response.data.data
      }

    });
  };

  $scope.timeLogs = {
    collection : [],
    get : function() {
      $http({
        method : 'GET',
        url : facultyServiceUrl + '/time_logs'
      }).then(function(response) {
        if (response.data.success) {
          $scope.timeLogs.collection = response.data.data
        }
      });
    }
  }

  $scope.canTime = {
    in : false,
    out : false
  };

  $scope.getAvailableTimeLogMethods = function() {

    $http({
      method : 'GET',
      url : facultyServiceUrl + '/time_logs/get_available_time_log_methods'
    }).then(function(response) {

      if (response.data.success) {
        $scope.canTime.in = !response.data.data.time_in;
        $scope.canTime.out = !response.data.data.time_out;
      }

    });

  }

  $scope.timeIn = function() {
    $http({
      method : 'POST',
      url : facultyServiceUrl + '/time_logs/time_in'
    }).then(function(response) {
      $scope.timeLogs.get();
      $scope.getAvailableTimeLogMethods();
    });
  };

  $scope.timeOut = function() {
    $http({
      method : 'POST',
      url : facultyServiceUrl + '/time_logs/time_out'
    }).then(function(response) {
      $scope.timeLogs.get();
      $scope.getAvailableTimeLogMethods();
    });
  };

  $scope.toTime = function(timeString){
    return (timeString != null) ? new Date( 'Jan 1 1970 ' + timeString) : '';
  };

  $scope.toDate = function(dateString) {
    return (dateString != null) ? new Date( dateString + ' 00:00:00') : '';
  }

  $scope.isCurrentDay = function(date) {
    var today = new Date();
    var theDate = new Date(date + ' 00:00:00');

    return today.toLocaleDateString() == theDate.toLocaleDateString();
  }

});
