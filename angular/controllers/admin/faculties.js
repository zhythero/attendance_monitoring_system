app.controller('facultiesController', function($scope, $http, FileUploader) {

	$scope.collection = [];

	$scope.uploader = new FileUploader({
		url : ''
	});

	$scope.createFacultyFormData = {};
	$scope.createFacultyFile = null;
	$scope.isCreating = false;

	$scope.createFacultyResponse = {
		success : null,
		message : ''
	}

	$scope.getCreateFacultyResponseClass = function(response) {
		switch (response) {
			case true:
				return 'alert alert-success';
				break;
			case false:
				return 'alert alert-danger';
				break;
			default:
				return '';
		}
	}

	// File Selection Event
	$scope.$on('fileSelected', function(e, args) {
		$scope.$apply(function() {
			$scope.createFacultyFile = args.file;
		});
	});

	// Create Faculty
	$scope.create = function() {

		$scope.isCreating = true;

		$http({
			method : 'POST',
			url : adminServiceUrl + '/faculties/create',
			headers : { 'Content-Type': undefined },
			data : {
				form_data : $scope.createFacultyFormData,
				file : $scope.createFacultyFile
			},
			transformRequest: function (data) {
				var formData = new FormData();
				formData.append("form_data", angular.toJson(data.form_data));
				formData.append("file", data.file);
				return formData;
			}
		}).then(function(response) {

			$scope.createFacultyResponse.success = response.data.success;

			if (response.data.success) {
				$scope.createFacultyResponse.message = response.data.data.message;
				for (prop in $scope.createFacultyFormData) {
					$scope.createFacultyFormData[prop] = '';
				}
			} else {
				$scope.createFacultyResponse.message = response.data.data.errors;
			}

			$scope.get();
			$scope.isCreating = false;

		}, function() {
			$scope.isCreating = false;

		});
	}

	/*
	$scope.create = function() {

		$scope.isCreating = true;

		console.log($scope.createFacultyFormData);

		$http({
			method : 'POST',
			url : adminServiceUrl + '/faculties/create',
			data : $scope.createFacultyFormData
		}).then(function(response) {

			$scope.createFacultyResponse.success = response.data.success;

			if (response.data.success) {
				$scope.createFacultyResponse.message = response.data.data.message;
				for (prop in $scope.createFacultyFormData) {
					$scope.createFacultyFormData[prop] = '';
				}
			} else {
				$scope.createFacultyResponse.message = response.data.data.errors;
			}

			$scope.get();
			$scope.isCreating = false;

		}, function() {
			$scope.isCreating = false;

		});
	} */

	$scope.get = function() {

		$http({
			url : adminServiceUrl + '/faculties/get'
		})
		.then(function(response) {

			if (response.data.success) {
				$scope.collection = response.data.data;
			}

		});
	}

});
