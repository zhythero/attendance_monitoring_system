<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Model Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/libraries/config.html
 */
class CI_Model {

	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		log_message('info', 'Model Class Initialized');
	}

	// --------------------------------------------------------------------

	/**
	 * __get magic
	 *
	 * Allows models to access CI's loaded classes using the same
	 * syntax as controllers.
	 *
	 * @param	string	$key
	 */
	public function __get($key)
	{
		// Debugging note:
		//	If you're here because you're getting an error message
		//	saying 'Undefined Property: system/core/Model.php', it's
		//	most likely a typo in your model code.
		return get_instance()->$key;
	}

	/*
		Zhythero's modification for CI Models
	*/
	protected $table;
	protected $primary_key = 'id';

	protected $offset = 0;
	protected $limit = 10;

	public function set_offset($offset) {
		$this->offset = $offset;
	}

	public function set_limit($limit) {
		$this->limit = $limit;
	}

	public function get_table() {
		return $this->table;
	}

	public function get($fields = null, $where = null) {

		// Fields
		if ( !empty($fields) ) {
			if ( is_array($fields) ) {
				foreach ($fields as $field) {
					$this->db->select($field);
				}
			} else if ( is_string($fields) ) {
				$this->db->select($fields);
			}
		}

		// Where
		if ( !empty($where) ) {
			if ( is_array($where) ) {
				foreach ($where as $key => $element) {
					if (is_int($key)) {
						$this->db->where($element);
					} else {
						$this->db->where($key, $element);
					}
				}
			} else if ( is_string($where) ) {
				$this->db->where($where);
			}
		}

		// Execute
		$result = $this->db->get($this->table, $this->limit, $this->offset);

		return $result->result_array();

	}

	public function set($data, $where) {

		if (is_int($where)) {
			$where = ['id' => $where];
		}

		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($this->table);
	}

	public function create($data) {

		return $this->db->insert( $this->table, $data);

	}

	public function count($where) {

		// Where
		if ( !empty($where) ) {
			$this->db->where($where);
		}

		return $this->db->count_all_results($this->table);
	}

	public function get_compiled_query($fields = null, $where = null) {

		// Fields
		if ( !empty($fields) ) {
			if ( is_array($fields) ) {
				foreach ($fields as $field) {
					$this->db->select($field);
				}
			} else if ( is_string($fields) ) {
				$this->db->select($fields);
			}
		}

		// Where
		if ( !empty($where) ) {
			if ( is_array($where) ) {
				foreach ($where as $key => $element) {
					if (is_int($key)) {
						$this->db->where($element);
					} else {
						$this->db->where($key, $element);
					}
				}
			} else if ( is_string($where) ) {
				$this->db->where($where);
			}
		}

		// Execute
		return $this->db->get_compiled_select($this->table);

	}

}
